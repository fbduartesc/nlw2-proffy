<div align="center">
  <img alt="Proffy" src=".img/design.png" height="400px" />    
  <h1>Node.js | ReactJS | React Native</h1>

 :rocket: *Project made to connect students to teachers.*
  </div>

# :pushpin: Table of contents

- [Technologies](#computer-technologies)
- [How to run](#construction_worker-how-to-run)
- [License](#closed_book-license)

# :computer: Technologies

This project was made using the following technologies:

<ul>
  <li><a href="https://nodejs.org/en/docs/">NodeJs</a></li>
  <li><a href="https://www.typescriptlang.org/">Typescript</a></li>
  <li><a href="https://pt-br.reactjs.org/">React</a></li>
  <li><a href="https://reactnative.dev/">React Native</a></li>
  <li><a href="https://expo.io/">Expo</a></li>
</ul>

# :construction_worker: How to run

### :computer: Downloading project 

```bash
# Clone repository into your machine
$ git clone https://gitlab.com/fbduartesc/nlw2-proffy.git
```

### 💻 Running project on a web browser

```bash
# Go to project's web folder
$ cd web/

# Install dependencies
$ yarn install or npm install

# Run application
$ yarn start or npm run start
```

Application located on http://localhost:3000/.

### 💻 Running project API

```bash
# Go to project's server folder
$ cd server/

# Install dependencies
$ yarn install or npm install

# Configurando o banco de dados e criando as tabelas.
$ yarn knex:migrate # ou npm run knex:migrate

# Run application
$ yarn start or npm run start
```

Application located on http://localhost:3333/.

### 📱 Running project on mobile

To run the project on mobile you need a cellphone with the [expo](https://play.google.com/store/apps/details?id=host.exp.exponent) app instaled or an android/ios emulator.
<br />
After forking this repository and making a clone of it in your machine, run the following commands inside the project folder :

```bash
# Go to mobile folder
$ cd mobile/

# Install dependencies
$ yarn install  or npm install

# Run application
$ yarn start or npm run start
```

<!--You can read the resulting QRCode with [expo](https://play.google.com/store/apps/details?id=host.exp.exponent) or through an emulator.-->

# :closed_book: License

Released in 2020.

Made with passion by [Fabio Duarte de Souza](https://gitlab.com/fbduartesc) 🚀.
This project is under the [MIT license](https://gitlab.com/fbduartesc/nlw2-proffy/blob/master/LICENSE).
